---
title: Hello Docussaurus.io
author: Valdyr Torres
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum dignissim ultricies. Fusce rhoncus ipsum tempor eros aliquam consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. 

<!--truncate -->

## Agora sim!

Vivamus elementum massa eget nulla aliquet sagittis. Proin odio tortor, vulputate ut odio in, ultrices ultricies augue. Cras ornare ultrices lorem malesuada iaculis. Etiam sit amet libero tempor, pulvinar mauris sed, sollicitudin sapien.

```
PS C:\devcode\gitlab\missaodevops> dir


    Diretório: C:\devcode\gitlab\missaodevops


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----        12/03/2021     18:38                docs
d-----        12/03/2021     17:53                website
-a----        12/03/2021     17:49             21 .dockerignore
-a----        12/03/2021     17:49            165 .gitignore
-a----        12/03/2021     17:49            497 docker-compose.yml
-a----        12/03/2021     17:49            145 Dockerfile
```